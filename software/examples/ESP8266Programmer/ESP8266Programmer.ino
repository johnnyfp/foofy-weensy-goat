#define WIFIPROG 14
#define WIFIEN   2
#define WIFIRST  15
#define OVERRIDE 7

long count;
boolean ord=0;
boolean prog=false;
boolean rst=false;

void setup() {
  Serial.begin(115200);
  Serial1.begin(115200);
  pinMode(OVERRIDE,INPUT_PULLUP);
  delay(5000);
  ord=digitalReadFast(OVERRIDE);
  pinMode(WIFIPROG,OUTPUT);
  pinMode(WIFIEN,OUTPUT);
  pinMode(WIFIRST,OUTPUT);
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWriteFast(LED_BUILTIN,ord);
  digitalWriteFast(WIFIPROG,ord);
  digitalWriteFast(WIFIRST,0);
  digitalWriteFast(WIFIEN,1);
  delay(100);
  digitalWriteFast(WIFIRST,1);
}

void loop() {
  if (digitalReadFast(OVERRIDE)) {
    if (prog==true) {
      Serial.println("Remote Mode");
      delay(200);
      prog=false;
    }
    if (Serial.available()) {
      digitalWriteFast(LED_BUILTIN,1);
      Serial1.write(Serial.read());
    } else digitalWriteFast(LED_BUILTIN,0);
  } else {
    if (prog==false) {
      Serial.println("Local Mode");
      delay(200);
      prog=true;
    }
    if (Serial.available()) {
      char r=Serial.read();
      switch (r){
        case 'r':
        Serial.println("Resetting");
        digitalWriteFast(WIFIRST,0);
        digitalWriteFast(WIFIEN,0);
        delay(1000);
        Serial.println("Starting Wifi");
        digitalWriteFast(WIFIEN,1);
        digitalWriteFast(WIFIRST,1);
        break;
        case 'p':
        Serial.println("Enable Programming");
        digitalWriteFast(WIFIPROG,0);
        break;
        case 'P':
        Serial.println("Disable Programming");
        digitalWriteFast(WIFIPROG,1);
        break;
        case 'e':
        Serial.println("Disable Wifi");
        digitalWriteFast(WIFIEN,0);
        break;
        case 'E':
        Serial.println("Enable Wifi");
        digitalWriteFast(WIFIEN,1);
        break;
      }
    }
  }
  if (Serial1.available()) {
      Serial.write(Serial1.read());
  }
}


