#define WIFIPROG 14
#define WIFIEN   2
#define WIFIRST  24

long count;

void setup() {
  Serial.begin(115200);
  Serial1.begin(115200);
  pinMode(WIFIPROG,OUTPUT);
  pinMode(WIFIEN,OUTPUT);
  pinMode(WIFIRST,OUTPUT);
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWriteFast(WIFIPROG,1);
  digitalWriteFast(WIFIRST,0);
  digitalWriteFast(WIFIEN,1);
  delay(100);
  do {
    Serial.println("Waiting for you before starting...\n\r");
    delay(1000);
  } while (!Serial.available());
  Serial.println("Setup Serial complete");
  delay(500);
  digitalWriteFast(WIFIRST,1);
  
  count=0;
}

void loop() {
  if (Serial.available()) {
    Serial1.write(Serial.read());
  }
  if (Serial1.available()) {
    Serial.write(Serial1.read());
  }
  count++;
  if (count==120000) digitalWriteFast(LED_BUILTIN,1);
  if (count==240000) {
    digitalWriteFast(LED_BUILTIN,0);
    count=0;
  }
}
